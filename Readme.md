# DAZN Test - Frontend
This is a Javascript UI for the TMDB Api. It is a single page app that allows the user to search and browse movies.

## System Requirements
- Node.js

## Libraries Used
- require.js
- jQuery
- crossroads.js
- hasher.js
- signals.js
- q.js

## Build and Run
`npm install`

`gulp`

## Dependency Injection with RequireJS
RequireJS is an AMD module loader that takes care of resolving the modules dependencies. In this case all the depencies were mapped in the app-config.js which effectively became the IoC container.

##### Optimizing RequireJS
RequireJS's config file (app-config.js) allows to map dependency names to file paths, in a few words every time a module is required the browser will fetch the .js file from the server. This impacts performance as the browser will perform multiple requests. 

For this reason the RequireJS team creaetd r.js. This libary will take care of bundling all the modules defined in app-config.js into one single (optionally minified) file therefore resolving the aformentioned issue.

## Changing Movie Service Provider
Thanks to the config features of Require.js it is very easy to swap implementations for a specific service. In this case I created a proxyApiService which connects to a localhost server which acts as a proxy to the TMDB API.

This is a good example of the 'L' principle in SOLID programming, where you can substitute an implementation without breaking the code.

In order to swap the implmentations you must replace one line in app-config.js:

`'moviesApiService': 'modules/services/tmdbApiService'`

with

`'moviesApiService': 'modules/services/proxyApiService'`

and then re-run `gulp`.
