var gulp = require('gulp');
var concat = require('gulp-concat');
var shell = require('gulp-shell');
var debug = require('gulp-debug');
var sass = require('gulp-sass');
var connect = require('gulp-connect');

gulp.task('copy-require', function(){
    return gulp.src('node_modules/requirejs/require.js').pipe(debug()).pipe(gulp.dest("dist/js/lib/requirejs"));
});

gulp.task('copy-html', function(){
    return gulp.src('src/*.html').pipe(gulp.dest("dist"));
});

gulp.task('compile-global', shell.task([
    'pwd',
    'node_modules/requirejs/bin/r.js -o src/assets/js/build-config.js'
]));

gulp.task('sass', function () {
  return gulp.src('./src/assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('connect', function () {
  connect.server({
    root: ['dist'],
    port: 8111
  });
});

gulp.task('default', ['copy-html', 'sass', 'copy-require', 'compile-global', 'connect']);
