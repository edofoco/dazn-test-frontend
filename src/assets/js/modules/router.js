'use strict'

define(['crossroads', 'controllersFactory', 'hasher'], function(crossroads, controllersFactory, hasher){

    var initController = function(name, params){
        var controller = controllersFactory.getInstance(name);
        controller.init(params);
    }

    function parseHash(newHash, oldHash){
        crossroads.parse(newHash);
    }

    var initHasher = function(){
        hasher.initialized.add(parseHash); 
        hasher.changed.add(parseHash); 
        hasher.init(); 
    };

    var initCrossroads = function(){
        crossroads.addRoute("", function () {
            initController('home', null);
        });
    
        crossroads.addRoute("movies/{movieId}", function (movieId) {
            initController('movieDetail', movieId )
        });

        var route404 = crossroads.addRoute(':rest*:', function(){
            initController('error')
        });

    }

    var init = function(){
        initCrossroads();
        initHasher();
    }

    return {
        init: init
    }

});