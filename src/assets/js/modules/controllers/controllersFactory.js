'use strict'

define(function(require){

    var controllers = {
        'home' : require('homeController'),
        'movieDetail' : require('movieDetailController'),
        'error': require('errorController')
    };

    var getInstance = function(controller){
       try {
            return controllers[controller];
        } catch(error) {
            return controllers['error'];
        }
    };

    return {
        getInstance: getInstance
    };
});