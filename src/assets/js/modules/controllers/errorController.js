'use strict'

define(['jquery','appService',], function(jQuery, appService){
    
    var rootNode = '';
    
    var setRootElement = function(){
        var node = '#' + appService.getRootNodeId();
        rootNode = jQuery(node);
    };

    var render = function(){
        rootNode.empty();
        var html = '<h1>404 - This is probably not the page your\'e looking for.</h1>';
        rootNode.append(html);
    };
    
    var init = function(params){
        setRootElement();
        render();
    };

    return {
        init: init
    };
});