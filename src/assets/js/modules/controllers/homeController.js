'use strict'

define(['jquery','appService', 'moviesApiService'], function(jQuery, appService, moviesApiService){
    
    var rootNode = '';

    var settings = {
        inputs: {
            searchBoxId: '#search-box'
        },
        containers: {
            moviesId: '#movies-container'
        }
    };

    var movies = [];

    var setRootElement = function(){
        var node = '#' + appService.getRootNodeId();
        rootNode = jQuery(node);
    };

    var render = function(){
        rootNode.empty();
        var html = '<input type="text" id="search-box" placeholder="Type a movie in here"/>' +
                    '<div id="movies-container" class="movies-container"></div>';
        rootNode.append(html);
        renderMovies();
    };
    
    var renderMovies = function(){
        var html = '';
        for(var i = 0; i < movies.length -1; i++){
            html += '<a class="movie" href="/#/movies/' + movies[i].id + '">' +
                        '<img src="' + moviesApiService.imageBaseUrl() + movies[i].backdrop_path + '"' +
                        'onerror="if (this.src != \'http://drkeyurparmar.com/wp-content/uploads/2015/02/dummy-article-img-1.jpg\') this.src = \'http://drkeyurparmar.com/wp-content/uploads/2015/02/dummy-article-img-1.jpg\';"  />' +
                        '<h4>' + movies[i].title + '</h4>' +
                    '</a>';
        }
        jQuery(settings.containers.moviesId).empty();
        jQuery(settings.containers.moviesId).append(html);
    };

    var saveMovies = function(data){
        movies = data.results;
    };

    var searchMovie = function(q){
        moviesApiService.searchMovies(q)
            .then(saveMovies)
            .then(renderMovies);
    };

    var bindUiElements = function(){
        jQuery(settings.inputs.searchBoxId).on('keyup', function(){
            searchMovie(this.value);
        });
    };

    var init = function(params){
        setRootElement();
        render();
        bindUiElements();
    };

    return {
        init: init
    };
});