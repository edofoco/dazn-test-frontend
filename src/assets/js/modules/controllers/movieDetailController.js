'use strict'

define(['jquery','appService', 'moviesApiService'], function(jQuery, appService, moviesApiService){

    var rootNode = '';

     var setRootElement = function(){
        var node = '#' + appService.getRootNodeId();
        rootNode = jQuery(node);
    };

    var render = function(data){
        rootNode.empty();
        var html = '<div class="movie-detail-container" >' +
                        '<img class="movie-detail-image" src="' + moviesApiService.imageBaseUrl() + data.backdrop_path + '" onerror="if (this.src != \'http://drkeyurparmar.com/wp-content/uploads/2015/02/dummy-article-img-1.jpg\') this.src = \'http://drkeyurparmar.com/wp-content/uploads/2015/02/dummy-article-img-1.jpg\';"  />' +
                        '<h1 class="movie-title">' + data.title + '</h1>' +
                        '<h2 class="movie-tagline">' + data.tagline + '</h2>' +
                        '<p class="movie-description">' + data.overview + '</p>' + 
                    '</div>';
        rootNode.append(html);
    };

    var getMovie = function(id){
        moviesApiService.getMovie(id)
            .then(render);
    };

    var init = function(movieId){
        setRootElement();
        getMovie(movieId);
    };

    return {
        init: init
    };

});