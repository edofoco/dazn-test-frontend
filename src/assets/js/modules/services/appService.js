'use strict'

define(function(){

    var rootNodeId = '';

    var getRootNodeId = function(){
        return rootNodeId;
    };

    var setRootNodeId = function(node){
        rootNodeId = node;
    };

    return {
        setRootNodeId: setRootNodeId,
        getRootNodeId: getRootNodeId
    };
});