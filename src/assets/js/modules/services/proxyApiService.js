'use strict'

define(['jquery', 'q'], function(jQuery, Q){
   
    var settings = {
        baseUri: 'http://localhost:5000',
        dataType: 'json',
        resources: {
            search: '/api/movies',
            details: '/api/movies'
        },
        imageBaseUrl:  'http://image.tmdb.org/t/p/w500/'

    };

    var makeRequest = function(endpoint, method, payload){
        return Q(jQuery.ajax(endpoint , {
            type: method,
            data: payload,
            error: function(error){
                console.log(error);
            },
            dataType: settings.dataType,
        }));
    };

    var searchMovies = function(query){
        var endpoint = settings.baseUri + settings.resources.search + '?query=' + query;
        return makeRequest(endpoint, 'GET', null);
    };

    var getMovie = function(id){
        var endpoint = settings.baseUri + settings.resources.details + '/' + id + '?api_key=' + settings.apiKey;
        return makeRequest(endpoint, 'GET', null);
    };

    var imageBaseUrl = function(){
        return settings.imageBaseUrl;
    };

    return{
        searchMovies: searchMovies,
        getMovie: getMovie,
        imageBaseUrl: imageBaseUrl
    };
});