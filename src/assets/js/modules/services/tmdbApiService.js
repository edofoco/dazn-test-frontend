'use strict'

define(['jquery', 'q'], function(jQuery, Q){
   
    var settings = {
        baseUri: 'https://api.themoviedb.org',
        apiKey: 'b50a81326ac27426cda5ead94ebe4354',
        dataType: 'json',
        resources: {
            search: '/3/search/movie',
            details: '/3/movie'
        },
        imageBaseUrl:  'http://image.tmdb.org/t/p/w500/'
    };

    var makeRequest = function(endpoint, method, payload){
        return Q(jQuery.ajax(endpoint , {
            type: method,
            data: payload,
            error: function(error){
                console.log(error);
            },
            dataType: settings.dataType,
        }));
    };

    var searchMovies = function(query){
        var endpoint = settings.baseUri + settings.resources.search + '?api_key=' + settings.apiKey + '&query=' + query;
        return makeRequest(endpoint, 'GET', null);
    };

    var getMovie = function(id){
        var endpoint = settings.baseUri + settings.resources.details + '/' + id + '?api_key=' + settings.apiKey;
        return makeRequest(endpoint, 'GET', null);
    };

    var imageBaseUrl = function(){
        return settings.imageBaseUrl;
    };
    
    return{
        searchMovies: searchMovies,
        imageBaseUrl: imageBaseUrl,
        getMovie: getMovie
    };
});