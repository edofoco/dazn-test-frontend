'use strict'

require.config({
    paths: {
        'q': '../../../node_modules/q/q',
        'jquery': '../../../node_modules/jquery/dist/jquery',
        'signals': '../../../node_modules/signals/dist/signals',
        'hasher': '../../../node_modules/hasher/dist/js/hasher',
        'crossroads': '../../../node_modules/crossroads/dist/crossroads',
        'moviesApiService': 'modules/services/tmdbApiService',
        'controllersFactory': 'modules/controllers/controllersFactory',
        'homeController': 'modules/controllers/homeController',
        'errorController': 'modules/controllers/errorController',
        'movieDetailController': 'modules/controllers/movieDetailController',
        'router': 'modules/router',
        'appService': 'modules/services/appService',
        'app': 'app'
    }
});

require(['app'], function(app){
    app.init('app');
});