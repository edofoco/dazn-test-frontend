'use strict'

define(['appService', 'router'], function(appService, router) {
    
    var init = function(rootNodeId){
        appService.setRootNodeId(rootNodeId);
        router.init();
    };

    return {
        init: init
    };
});
